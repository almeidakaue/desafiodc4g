## Sobre este repositório
Este projeto é um desafio proposto pela empresa DC4G.

A aplicação React é um clone deste repositório do [GitHub](https://github.com/juniorsnts/facedev).

### O desafio
O objetivo do desafio elaborado pela DC4G consiste em criar uma Dockerfile para a aplicação e subir a imagem do container para o Docker hub através de uma pipepline

#### Dockerfile
O primeiro passo que eu realizei, foi entender a aplicação React e ver quais seriam suas depêndencias para que eu pudesse fazer uma Dockerfile, que executaria a aplicação sem problemas. Após isso, elaborei um arquivo e indiquei que a imagem utilizada seria o **node v 16.15**, logo criei um diretório próprio para a sua execução, em seguida, determinei este diretório como principal diretório de trabalho. Copiei os arquivos da aplicação para dentro do diretório criado anteriormente e passei as intruções para o executar o comando **npm install** e liberar acesso para a aplicação através da **porta 3000** do container e por fim indiquei que o comando a ser executado durante a inicialização do container deveria ser **npm start** para iniciar a aplicação React.

#### Desenvolvimento da pipeline
Como antes, nunca havia trabalhado com pipelines e serviços de integração (CI/CD) precisei antes dar uma lida na documentação do próprio **gitlab** para entender como realizar uma pipeline em sí. O primeiro passo foi entender a estrutura do arquivo **.gitlab-ci.yml**, após isso, dei uma olhada em variáveis de CI/CD para ver o seu uso e vantagens relacionados a questões de segurança da pipeline, como ocultar informações de login.

Após entender como funciona a estrutura de uma pipeline, resolvi partir para a prática, foi ae que começaram os desafios. A principal dificuldade foi descobrir quais etapas da pipeline seriam efetivas e quais apenas estariam deixando o processo mais demorado.

Os primeiros arquivos que escrevi consistiam em três etapas, em cada etapa era realizado uma etapa de construção do CI/CD, a primeira etapa criava o build da Dockerfile, a segunda etapa era apenas uma mensagem de teste, e a terceira etapa que seria o envio da imagem gerada para o docker hub. Nesta última etapa, tive problemas como conseguir especificar de forma efetiva qual seria a tag da imagem ao enviar, e isto fazia com que a pipeline não obtivesse êxito, após algumas horas estudando sobre **CI/CD e estruturação do arquivo .gitlab-ci.yml**, percebi que podia realizar tanto o build da Dockerfile quanto o envio da imagem para o docker hub com apenas uma etapa na pipeline, não sendo necessário etapas adicionais que só fariam a pipeline ser mais demorada e um pouco mais complexa. 

Por fim, a pipeline final ficou com apenas uma etapa responsável por realizar o build e o envio da imagem ao docker hub, tendo assim, um processo de integração mais rápido (visto que só precisa realizar uma etapa) e uma de estruturação do arquivo de CI/CD menos complexa, facilitando também o trabalho de outras pessoas que no futuro possam a vir trabalhar no mesmo projeto, e a manutenção da pipeline ao longo do tempo mais simples.

### Conclusão
Acredito que este desafio foi de grande valor para mim, através dele tive a oportunidade de estudar ferramentas novas e entender de forma prática alguns conceitos da área **DevOps**, além de aprender um pouco mais sobre a plataforma **gitlab**, tive um desafio prático de criar uma pipeline de integração e ver na prática o conceito de **CI/CD**. Concluo este desafio com muita satisfação, tanto como estudante da área de tecnologia como profissional, senti que consegui colocar em prática diversas ferramentas já antes conhecidas por mim. 

O link para a imagem no **docker hub** se encontra [aqui](https://hub.docker.com/repository/docker/almeidakaue/facedev).

#### Formas de contato

Meu perfil no [GitHub](https://github.com/kakoalmeida).

Meu [LinkedIn](https://www.linkedin.com/in/kauealmeida99/).

Caso queira me contatar de outra maneira, pode enviar um e-mail para, kako.rd.ad@gmail.com.

